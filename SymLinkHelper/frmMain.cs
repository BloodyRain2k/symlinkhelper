﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
/* not working runtime UAC stuff
using System.Security.Principal;
using Microsoft.Win32.SafeHandles;
using SimpleImpersonation;
//*/

namespace SymLinkHelper
{
    public partial class frmMain : Form
    {
        bool selectingOriginal, validFile, validDirectory;

        /// <span class="code-SummaryComment"><summary></span>
        /// Executes a shell command synchronously.
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="command">string command</param></span>
        /// <span class="code-SummaryComment"><returns>string, as output of the command.</returns></span>
        public string ExecuteCommandSync(string command, bool admin = false) {  // https://www.codeproject.com/Articles/25983/How-to-Execute-a-Command-in-C
            try {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                var link_log = $"{new FileInfo(Application.ExecutablePath).Directory.FullName}\\Created_Links.log";
                var admin_result_file = $"{new FileInfo(Application.ExecutablePath).Directory.FullName}\\admin_result.log";
                ProcessStartInfo procStartInfo =
                    new ProcessStartInfo("cmd", $"/c {command} {(admin ? $" > \"{admin_result_file}\" 2>&1" : "")}") {
                        // redirecting the output is only possible without UseShellExecute
                        //UseShellExecute = false,
                        //RedirectStandardOutput = true,
                        //RedirectStandardError = true,
                        UseShellExecute = admin, // false
                        RedirectStandardOutput = !admin, // true
                        RedirectStandardError = !admin, // true
                        CreateNoWindow = true,
                        Verb = (admin ? "runas" : ""),
                    };

                // Now we create a process, assign its ProcessStartInfo and start it
                Process proc = new Process() {
                    StartInfo = procStartInfo,
                };
                proc.Start();
                // Get the output into a string
                string result = "";
                if (admin) {
                    result = File.ReadAllText(admin_result_file);
                    File.Delete(admin_result_file);
                }
                else {
                    result = proc.StandardOutput.ReadToEnd();
                    if (result == "") {
                        result = "Error: " + proc.StandardError.ReadToEnd();
                        result = ExecuteCommandSync(command, true);

                        /* screw this runtime UAC crap...
                        var frmPW = new frmPassword();
                        if (frmPW.ShowDialog() == DialogResult.OK) {
                            var current = WindowsIdentity.GetCurrent();
                            UserCredentials credentials = new UserCredentials(current.Name, frmPW.password);
                            frmPW.Dispose();
                            result = "";
                            Impersonation.RunAsUser(credentials, LogonType.Interactive, (userHandle) => {
                                using (WindowsImpersonationContext context = WindowsIdentity.Impersonate(userHandle.DangerousGetHandle())) {
                                    result = ExecuteCommandSync(command, true);
                                }
                            });
                        } //*/
                    }
                }
                Debug.WriteLine(result);

                if (!result.EndsWith("\r\n")) { result += "\r\n"; }
                File.AppendAllText(link_log, result);

                return result;
            }
            catch (Exception objException) {
                Debug.WriteLine(objException.Message);
                Debug.WriteLine(objException.StackTrace);
                return objException.Message;
            }
        }

        private void validatePaths() {
            optDefault.Checked = validFile = File.Exists(txtOriginal.Text);

            btnCreate.Enabled = (validFile || validDirectory) && txtLinked.Text.Trim() != "";

            optDirectoryJunction.Checked = validDirectory = Directory.Exists(txtOriginal.Text);
            optDirectoryJunction.Enabled = optDirectoryLink.Enabled = validDirectory;

            optDefault.Enabled = optHardLink.Enabled = btnCreate.Enabled;
        }


        public frmMain() {
            InitializeComponent();

            Text += $" - {Application.ProductVersion}";
        }

        private void btnSelect_Click(object sender, EventArgs e) {
            selectingOriginal = ((Button)sender) == btnOriginal; // which button was clicked?
            fbdFolder.Description = "Select " + (selectingOriginal ? "original" : "target") + " directory for linking";

            if (optDirectoryLink.Checked) {
                if (fbdFolder.ShowDialog() == DialogResult.OK) {
                    if (selectingOriginal) {
                        txtOriginal.Text = fbdFolder.SelectedPath;
                    }
                    else {
                        txtLinked.Text = fbdFolder.SelectedPath;
                    }
                }
            }
            else {
                if (selectingOriginal) {
                    if (ofdOriginalFile.ShowDialog() == DialogResult.OK) {
                        txtOriginal.Text = ofdOriginalFile.FileName;
                    }
                }
                else {
                    if (sfdLinkedFile.ShowDialog() == DialogResult.OK) {
                        txtLinked.Text = sfdLinkedFile.FileName;
                    }
                }
            }
        }

        private void btnCreate_Click(object sender, EventArgs e) {
            var link_path = txtLinked.Text.Replace("\\*", "\\" + txtOriginal.Text.Substring(txtOriginal.Text.LastIndexOf("\\") + 1));
            var cmd = "mklink";
            cmd += (optDirectoryLink.Checked ? " /d " : "");
            cmd += (optHardLink.Checked ? " /h " : "");
            cmd += (optDirectoryJunction.Checked ? " /j " : "");
            cmd += " \"" + link_path + "\" \"" + txtOriginal.Text + "\"";

            try {
                if (Directory.Exists(link_path)) {
                    var dir = new DirectoryInfo(link_path);

                    if (!dir.Attributes.HasFlag(FileAttributes.ReparsePoint) && dir.GetDirectories().Length + dir.GetFiles().Length == 0) {
                        dir.Delete(true); // directory empty so don't bother asking
                    }
                    else if (MessageBox.Show("A symbolic link can only be created if the folder to be linked from doesn't already exist." +
                        "\n\nDelete \"" + link_path + "\" to proceed?", "Link target already exists", MessageBoxButtons.OKCancel) == DialogResult.OK) {

                        //Directory.Delete(link_path, true);
                        dir.Delete(true);
                    }
                    else {
                        return; // abort
                    }
                }
                else if (File.Exists(link_path)) {
                    if (MessageBox.Show("A symbolic link can only be created if the file to be linked from doesn't already exist." +
                        "\n\nDelete \"" + link_path + "\" to proceed?", "Link target already exists", MessageBoxButtons.OKCancel) == DialogResult.OK) {
                        File.Delete(link_path);
                    }
                    else {
                        return; // abort
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show($"Something went wrong while deleting the existing linked {(optDirectoryLink.Checked ? "directory" : "file")}."
                    + $"\n\n{ex.Message}", "Error", MessageBoxButtons.OK);
                return;
            }

            MessageBox.Show(ExecuteCommandSync(cmd), "Result", MessageBoxButtons.OK);
        }

        private string ValidateDrag(DragEventArgs evt) {
            var types = new[] { DataFormats.FileDrop, DataFormats.SymbolicLink };

            if (evt.Data.GetDataPresent(DataFormats.FileDrop)) {
                var fileDir = (evt.Data.GetData(DataFormats.FileDrop) as string[])[0];

                var isDir = Directory.Exists(fileDir);
                var isFile = File.Exists(fileDir);

                if (!isDir && !isFile) { // it's neither a file nor a directory, how'd this happen?
                    Console.WriteLine("Something exploded...");
                    return null;
                }

                return fileDir;
            }

            else {
                //MessageBox.Show(string.Join("\n", evt.Data.GetFormats()), "Detected", MessageBoxButtons.OK);
            }

            return null;
        }

        private void txt_TextChanged(object sender, EventArgs e) {
            validatePaths();
        }

        private void txt_DragEnter(object sender, DragEventArgs e) {
            if (ValidateDrag(e) != null) { // verify that the dragged data is usable
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void txt_DragDrop(object sender, DragEventArgs e) {
            var str = ValidateDrag(e) ?? "";
            if (str != "") {
                if (sender == txtOriginal) {
                    txtOriginal.Text = str;
                }
                else {
                    var org = txtOriginal.Text;
                    org = org.Substring(org.LastIndexOf("\\") + 1);

                    var lnk = str;
                    lnk = lnk.Substring(lnk.LastIndexOf("\\") + 1);

                    txtLinked.Text = str;

                    if (Directory.Exists(str) && org.ToLower() != lnk.ToLower()) {
                        // the target directory exists but isn't the same as the original, so we assume it's the parent of the to-be-linked
                        txtLinked.Text += "\\*";
                    }
                }
            }

            validatePaths();
        }
    }
}
