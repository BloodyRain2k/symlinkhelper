﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SymLinkHelper
{
    public partial class frmPassword : Form
    {
        public string password;

        public frmPassword() {
            InitializeComponent();
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e) {
            switch (e.KeyCode) {
                case Keys.Enter:
                    password = txtPassword.Text;
                    DialogResult = DialogResult.OK;
                    Close();
                    break;

                case Keys.Escape:
                    DialogResult = DialogResult.Cancel;
                    Close();
                    break;
            }
        }
    }
}
