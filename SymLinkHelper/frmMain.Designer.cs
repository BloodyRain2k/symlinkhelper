﻿namespace SymLinkHelper
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtOriginal = new System.Windows.Forms.TextBox();
            this.txtLinked = new System.Windows.Forms.TextBox();
            this.btnOriginal = new System.Windows.Forms.Button();
            this.btnLinked = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.fbdFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.sfdLinkedFile = new System.Windows.Forms.SaveFileDialog();
            this.ofdOriginalFile = new System.Windows.Forms.OpenFileDialog();
            this.optDirectoryJunction = new System.Windows.Forms.RadioButton();
            this.optHardLink = new System.Windows.Forms.RadioButton();
            this.optDirectoryLink = new System.Windows.Forms.RadioButton();
            this.optDefault = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // txtOriginal
            // 
            this.txtOriginal.AllowDrop = true;
            this.txtOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOriginal.Location = new System.Drawing.Point(130, 12);
            this.txtOriginal.Name = "txtOriginal";
            this.txtOriginal.Size = new System.Drawing.Size(482, 20);
            this.txtOriginal.TabIndex = 1;
            this.txtOriginal.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtOriginal.DragDrop += new System.Windows.Forms.DragEventHandler(this.txt_DragDrop);
            this.txtOriginal.DragEnter += new System.Windows.Forms.DragEventHandler(this.txt_DragEnter);
            // 
            // txtLinked
            // 
            this.txtLinked.AllowDrop = true;
            this.txtLinked.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLinked.Location = new System.Drawing.Point(130, 39);
            this.txtLinked.Name = "txtLinked";
            this.txtLinked.Size = new System.Drawing.Size(482, 20);
            this.txtLinked.TabIndex = 3;
            this.txtLinked.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtLinked.DragDrop += new System.Windows.Forms.DragEventHandler(this.txt_DragDrop);
            this.txtLinked.DragEnter += new System.Windows.Forms.DragEventHandler(this.txt_DragEnter);
            // 
            // btnOriginal
            // 
            this.btnOriginal.Location = new System.Drawing.Point(11, 11);
            this.btnOriginal.Name = "btnOriginal";
            this.btnOriginal.Size = new System.Drawing.Size(113, 22);
            this.btnOriginal.TabIndex = 0;
            this.btnOriginal.Text = "&Original File / Folder";
            this.btnOriginal.UseVisualStyleBackColor = true;
            this.btnOriginal.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnLinked
            // 
            this.btnLinked.Location = new System.Drawing.Point(11, 38);
            this.btnLinked.Name = "btnLinked";
            this.btnLinked.Size = new System.Drawing.Size(113, 22);
            this.btnLinked.TabIndex = 2;
            this.btnLinked.Text = "&Linked File / Folder";
            this.btnLinked.UseVisualStyleBackColor = true;
            this.btnLinked.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreate.Enabled = false;
            this.btnCreate.Location = new System.Drawing.Point(511, 65);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(102, 22);
            this.btnCreate.TabIndex = 7;
            this.btnCreate.Text = "&Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // sfdLinkedFile
            // 
            this.sfdLinkedFile.Title = "Select linking target file";
            // 
            // ofdOriginalFile
            // 
            this.ofdOriginalFile.FileName = "*.*";
            this.ofdOriginalFile.ReadOnlyChecked = true;
            this.ofdOriginalFile.Title = "Select original file for linking";
            // 
            // optDirectoryJunction
            // 
            this.optDirectoryJunction.AutoSize = true;
            this.optDirectoryJunction.Checked = true;
            this.optDirectoryJunction.Enabled = false;
            this.optDirectoryJunction.Location = new System.Drawing.Point(339, 68);
            this.optDirectoryJunction.Name = "optDirectoryJunction";
            this.optDirectoryJunction.Size = new System.Drawing.Size(166, 17);
            this.optDirectoryJunction.TabIndex = 8;
            this.optDirectoryJunction.TabStop = true;
            this.optDirectoryJunction.Text = "Directory &Junction [/j] (Server)";
            this.optDirectoryJunction.UseVisualStyleBackColor = true;
            // 
            // optHardLink
            // 
            this.optHardLink.AutoSize = true;
            this.optHardLink.Enabled = false;
            this.optHardLink.Location = new System.Drawing.Point(82, 68);
            this.optHardLink.Name = "optHardLink";
            this.optHardLink.Size = new System.Drawing.Size(91, 17);
            this.optHardLink.TabIndex = 9;
            this.optHardLink.Text = "&Hard Link [/h]";
            this.optHardLink.UseVisualStyleBackColor = true;
            // 
            // optDirectoryLink
            // 
            this.optDirectoryLink.AutoSize = true;
            this.optDirectoryLink.Enabled = false;
            this.optDirectoryLink.Location = new System.Drawing.Point(184, 68);
            this.optDirectoryLink.Name = "optDirectoryLink";
            this.optDirectoryLink.Size = new System.Drawing.Size(144, 17);
            this.optDirectoryLink.TabIndex = 10;
            this.optDirectoryLink.Text = "&Directory Link [/d] (client)";
            this.optDirectoryLink.UseVisualStyleBackColor = true;
            // 
            // optDefault
            // 
            this.optDefault.AutoSize = true;
            this.optDefault.Enabled = false;
            this.optDefault.Location = new System.Drawing.Point(12, 68);
            this.optDefault.Name = "optDefault";
            this.optDefault.Size = new System.Drawing.Size(59, 17);
            this.optDefault.TabIndex = 11;
            this.optDefault.Text = "De&fault";
            this.optDefault.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 97);
            this.Controls.Add(this.optDefault);
            this.Controls.Add(this.optDirectoryLink);
            this.Controls.Add(this.optHardLink);
            this.Controls.Add(this.optDirectoryJunction);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.btnLinked);
            this.Controls.Add(this.btnOriginal);
            this.Controls.Add(this.txtLinked);
            this.Controls.Add(this.txtOriginal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sym Link Helper";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOriginal;
        private System.Windows.Forms.TextBox txtLinked;
        private System.Windows.Forms.Button btnOriginal;
        private System.Windows.Forms.Button btnLinked;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.FolderBrowserDialog fbdFolder;
        private System.Windows.Forms.SaveFileDialog sfdLinkedFile;
        private System.Windows.Forms.OpenFileDialog ofdOriginalFile;
        private System.Windows.Forms.RadioButton optDirectoryJunction;
        private System.Windows.Forms.RadioButton optHardLink;
        private System.Windows.Forms.RadioButton optDirectoryLink;
        private System.Windows.Forms.RadioButton optDefault;
    }
}

